/**
 * 
 */
package org.yah.tools.dropwizard.spring.example.service;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.codahale.metrics.Counter;


/**
 * @author Yah
 * @created 2019/04/25
 */
public class SimpleEntityRepository {

	private final AtomicInteger nextId = new AtomicInteger(1);

	private final Map<Integer, SimpleEntity> entities = new LinkedHashMap<>();

	private final Counter counterMetrics;

	public SimpleEntityRepository() {
		this(null);
	}

	public SimpleEntityRepository(Counter counterMetrics) {
		this.counterMetrics = counterMetrics;
	}

	public SimpleEntity findById(int id) {
		SimpleEntity res = entities.get(id);
		if (res == null)
			throw new NoSuchElementException("Entity " + id + " was not found");
		return res;
	}

	public SimpleEntity create(String name) {
		SimpleEntity entity = SimpleEntity.builder().withId(nextId.getAndIncrement()).withName(name).build();
		entities.put(entity.getId(), entity);
		counterMetrics.inc();
		return entity;
	}

	public void update(SimpleEntity entity) {
		if (entities.computeIfPresent(entity.getId(), (id, previous) -> entity) == null)
			throw new NoSuchElementException("Entity " + entity.getId() + " was not found");
	}

	public void delete(int id) {
		if (entities.remove(id) == null)
			throw new NoSuchElementException("Entity " + id + " was not found");
		counterMetrics.dec();
	}

	public List<SimpleEntity> findByName(String name) {
		return entities.values()
		        .stream()
		        .filter(e -> e.getName().equals(name))
		        .collect(Collectors.toList());
	}

	public Collection<SimpleEntity> getAll() {
		return entities.values();
	}

}
