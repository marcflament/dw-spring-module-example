package org.yah.tools.dropwizard.spring.example.resource;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class CreateRequest {

	@NotBlank
	private final String name;

	@JsonCreator
	public CreateRequest(@JsonProperty("name") String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "CreateRequest [name=" + name + "]";
	}

}
