package org.yah.tools.dropwizard.spring.example.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.yah.tools.dropwizard.spring.example.RandomHealthCheck;
import org.yah.tools.dropwizard.spring.example.SimpleRestConfiguration;
import org.yah.tools.dropwizard.spring.example.resource.SimpleEntityResource;
import org.yah.tools.dropwizard.spring.example.service.SimpleEntityRepository;

import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;


@Configuration
public class SimpleRestSpringConfiguration {

	@Bean
	public SimpleEntityRepository repository(MetricRegistry metricRegistry) {
		Counter metricsCounter = new Counter();
		SimpleEntityRepository res = new SimpleEntityRepository(metricsCounter);
		res.create("First value");
		res.create("Second value");
		res.create("Third value");
		res.create("And again another value");
		res.create("Still more value");
		res.create("The last value");
		
		metricRegistry.register(SimpleEntityRepository.class.getName(), metricsCounter);
		
		return res;
	}

	@Bean
	public SimpleEntityResource resource(SimpleEntityRepository repository) {
		return new SimpleEntityResource(repository);
	}

	@Bean
	public RandomHealthCheck healthcheck(SimpleRestConfiguration configuration) {
		return new RandomHealthCheck(configuration.getHealthyChancePct());
	}

}
