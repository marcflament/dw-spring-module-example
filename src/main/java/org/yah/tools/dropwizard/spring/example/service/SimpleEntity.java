/**
 * 
 */
package org.yah.tools.dropwizard.spring.example.service;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


/**
 * @author Yah
 * @created 2019/04/25
 */
@JsonDeserialize(builder = SimpleEntity.Builder.class)
public class SimpleEntity {

	private final int id;

	private final String name;

	private SimpleEntity(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "SimpleEntity [id=" + id + ", name=" + name + "]";
	}

	public static Builder builder() {
		return new Builder();
	}

	public static Builder builderFrom(SimpleEntity e) {
		return new Builder(e);
	}

	public static class Builder {
		private int id;
		private String name;

		private Builder() {}

		private Builder(SimpleEntity e) {
			this.id = e.id;
			this.name = e.name;
		}

		public Builder withId(int id) {
			this.id = id;
			return this;
		}

		public Builder withName(String name) {
			this.name = name;
			return this;
		}

		public SimpleEntity build() {
			return new SimpleEntity(id, name);
		}
	}
}
