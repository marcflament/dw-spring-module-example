/**
 * 
 */
package org.yah.tools.dropwizard.spring.example.resource;

import java.util.Collection;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.yah.tools.dropwizard.spring.example.service.SimpleEntity;
import org.yah.tools.dropwizard.spring.example.service.SimpleEntityRepository;


/**
 * @author Yah
 * @created 2019/04/25
 */
@Path("/simple")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SimpleEntityResource {

	private final SimpleEntityRepository repository;

	public SimpleEntityResource(SimpleEntityRepository repository) {
		this.repository = repository;
	}

	@GET
	public Collection<SimpleEntity> getAll() {
		return repository.getAll();
	}

	@GET
	@Path("{id}")
	public SimpleEntity get(@PathParam("id") int id) {
		return repository.findById(id);
	}

	@POST
	public SimpleEntity create(@NotNull @Valid CreateRequest request) {
		return repository.create(request.getName());
	}

	@PUT
	@Path("{id}")
	public void update(@PathParam("id") int id, @NotNull @Valid UpdateRequest request) {
		SimpleEntity entity = SimpleEntity.builder().withId(id).withName(request.getName()).build();
		repository.update(entity);
	}

	@DELETE
	@Path("{id}")
	public void delete(@PathParam("id") int id) {
		repository.delete(id);
	}

	@POST
	@Path("/search")
	public Collection<SimpleEntity> search(@Valid @NotNull SearchRequest searchRequest) {
		return repository.findByName(searchRequest.getName());
	}
}
