package org.yah.tools.dropwizard.spring.example.resource;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class UpdateRequest {

	@NotBlank
	private final String name;

	@JsonCreator
	public UpdateRequest(@JsonProperty("name") String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "UpdateRequest [name=" + name + "]";
	}

}
