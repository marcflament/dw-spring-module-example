/**
 * 
 */
package org.yah.tools.dropwizard.spring.example;

/**
 * @author Yah
 * @created 2019/04/25
 */
public class SomeServiceProperties {
	
	private String host = "locahost";
	
	private int port = 8080;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}
