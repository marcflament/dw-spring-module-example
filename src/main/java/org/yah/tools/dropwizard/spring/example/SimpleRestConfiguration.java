/**
 * 
 */
package org.yah.tools.dropwizard.spring.example;

import javax.annotation.Nonnegative;

import io.dropwizard.Configuration;


/**
 * @author Yah
 * @created 2019/04/25
 */
public class SimpleRestConfiguration extends Configuration {

	private final SomeServiceProperties serviceProperties = new SomeServiceProperties();

	@Nonnegative
	private float healthyChancePct = 0.9f;

	public SomeServiceProperties getServiceProperties() {
		return serviceProperties;
	}

	public float getHealthyChancePct() {
		return healthyChancePct;
	}

	public void setHealthyChancePct(float healthyChancePct) {
		this.healthyChancePct = healthyChancePct;
	}

}
