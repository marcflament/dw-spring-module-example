/**
 * 
 */
package org.yah.tools.dropwizard.spring.example;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.yah.tools.dropwizard.spring.example.spring.SimpleRestSpringConfiguration;
import org.yah.tools.dropwizard.springbundle.SpringBundle;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;


/**
 * @author Yah
 * @created 2019/04/25
 */
public class SimpleRestServer extends Application<SimpleRestConfiguration> {

	public static void main(String[] args) throws Exception {
		new SimpleRestServer().run(args);
	}

	@Override
	public String getName() {
		return "simple-rest";
	}

	@Override
	public void initialize(Bootstrap<SimpleRestConfiguration> bootstrap) {
		bootstrap.addBundle(new AssetsBundle("/assets/", "/", "index.html"));

		SpringBundle<SimpleRestConfiguration> springModule = SpringBundle
		        .<SimpleRestConfiguration>builder(SimpleRestSpringConfiguration.class)
		        .withParentConfigurer(this::configureDropwizardBeanFactory)
		        .build();
		bootstrap.addBundle(springModule);
	}

	@Override
	public void run(SimpleRestConfiguration configuration, Environment environment) throws Exception {
		// thank you spring
	}

	private void configureDropwizardBeanFactory(ConfigurableBeanFactory beanFactory,
	        SimpleRestConfiguration configuration,
	        Environment environment) {
		beanFactory.registerSingleton("metrics", environment.metrics());
	}
}
