/**
 * 
 */
package org.yah.tools.dropwizard.spring.example;

import java.util.Random;

import com.codahale.metrics.health.HealthCheck;


/**
 * @author Yah
 * @created 2019/04/25
 */
public class RandomHealthCheck extends HealthCheck {

	private final Random random = new Random();

	private final float healthyChancePct;

	public RandomHealthCheck(float healthyChancePct) {
		this.healthyChancePct = healthyChancePct;
	}

	@Override
	protected Result check() throws Exception {
		if (random.nextFloat() < healthyChancePct) {
			return Result.healthy("Lucky you");
		}
		return Result.unhealthy("Too bad, try again");
	}

}
